Source: py-macaroon-bakery
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Colin Watson <cjwatson@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-fixtures,
 python3-httmock,
 python3-nacl,
 python3-protobuf,
 python3-pymacaroons,
 python3-pytest,
 python3-requests,
 python3-rfc3339,
 python3-setuptools,
 python3-six,
Standards-Version: 4.1.1
Vcs-Git: https://salsa.debian.org/python-team/packages/py-macaroon-bakery.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/py-macaroon-bakery
Homepage: https://github.com/go-macaroon-bakery/py-macaroon-bakery
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild
X-Style: black

Package: python3-macaroonbakery
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Higher-level macaroon operations for Python 3
 Macaroons, like cookies, are a form of bearer credential.  Unlike opaque
 tokens, macaroons embed caveats that define specific authorization
 requirements for the target service, the service that issued the root
 macaroon and which is capable of verifying the integrity of macaroons it
 receives.
 .
 Macaroons allow for delegation and attenuation of authorization.  They are
 simple and fast to verify, and decouple authorization policy from the
 enforcement of that policy.
 .
 The macaroonbakery library builds on pymacaroons to allow working with
 macaroons at a higher level, such as by automatically gathering discharge
 macaroons for third-party caveats from their corresponding services.
